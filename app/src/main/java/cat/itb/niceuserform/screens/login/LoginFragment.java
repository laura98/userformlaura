package cat.itb.niceuserform.screens.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.niceuserform.R;

public class LoginFragment extends Fragment {
    @BindView(R.id.loginRegisterBtn)
    Button registerBtn;
    @BindView(R.id.loginBackgroundImg)
    ImageView loginBackgroundImg;
    @BindView(R.id.loginTextView)
    TextView loginTextView;
    @BindView(R.id.loginName)
    TextInputEditText loginName;
    @BindView(R.id.loginUsernameInput)
    TextInputLayout loginUsernameInput;
    @BindView(R.id.loginPassword)
    TextInputEditText loginPassword;
    @BindView(R.id.loginPasswordInput)
    TextInputLayout loginPasswordInput;
    @BindView(R.id.loginLoginBtn)
    MaterialButton loginLoginBtn;
    @BindView(R.id.forgotPasswordBtn)
    MaterialButton forgotPasswordBtn;

    private LoginViewModel mViewModel;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.loginRegisterBtn)
    public void onLoginRegisterClicked() {
        NavDirections action = LoginFragmentDirections.loginToRegister();
        Navigation.findNavController(getView()).navigate(action);

    }
    @OnClick(R.id.loginLoginBtn)
    public void onLogin(){
        if(loginAccept()){
            NavDirections action = LoginFragmentDirections.loginToLogged();
            Navigation.findNavController(getView()).navigate(action);
        }
    }

    private boolean loginAccept() {
        boolean allDone = true;
        if(loginName.getText().toString().isEmpty()){
            loginUsernameInput.setError("Field Required");
            allDone = false;
        }else loginUsernameInput.setError(null);
        if (loginPassword.getText().toString().isEmpty()){
            loginPasswordInput.setError("Field Required");
            allDone = false;
        }else loginPasswordInput.setError(null);
        return allDone;
    }

}
