package cat.itb.niceuserform.screens.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.niceuserform.R;

public class RegisterFragment extends Fragment {
    @BindView(R.id.registerLoginBtn)
    Button loginBtn;
    @BindView(R.id.registerBackgroundImg)
    ImageView registerBackgroundImg;
    @BindView(R.id.registerTextView)
    TextView registerTextView;
    @BindView(R.id.registerUsernameInput)
    TextInputLayout registerUsernameInput;
    @BindView(R.id.registerPasswordInput)
    TextInputLayout registerPasswordInput;
    @BindView(R.id.registerRepeatPasswordInput)
    TextInputLayout registerRepeatPasswordInput;
    @BindView(R.id.registerEmailInput)
    TextInputLayout registerEmailInput;
    @BindView(R.id.registerNameInput)
    TextInputLayout registerNameInput;
    @BindView(R.id.registerSurnamesInput)
    TextInputLayout registerSurnamesInput;
    @BindView(R.id.registerBirthDateInput)
    TextInputLayout registerBirthDateInput;
    @BindView(R.id.registerGenderInput)
    TextInputLayout registerGenderInput;
    @BindView(R.id.checkbox)
    CheckBox checkboxBind;
    @BindView(R.id.registerRegisterBtn)
    MaterialButton registerRegisterBtn;
    @BindView(R.id.BirthDate)
    TextInputEditText BirthDate;
    @BindView(R.id.gender)
    TextInputEditText gender;
    @BindView(R.id.usernameRegister)
    TextInputEditText usernameRegister;
    @BindView(R.id.passwordRegister)
    TextInputEditText passwordRegister;
    @BindView(R.id.repeatPassword)
    TextInputEditText repeatPassword;
    @BindView(R.id.email)
    TextInputEditText email;

    private RegisterViewModel mViewModel;

    private String[] optionsArray;

    public static RegisterFragment newinstance() {
        return new RegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


    }

    @OnClick(R.id.BirthDate)
    public void onBirthDateClicked() {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("birth date");
        MaterialDatePicker<Long> picker = builder.build();
        picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
        picker.show(getFragmentManager(), picker.toString());
    }

    private void doOnDateSelected(Long aLong) {

        BirthDate.setText(new Date(aLong).toString());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        // TODO: Use the ViewModel
    }

    @OnClick(R.id.registerLoginBtn)
    public void onRegisterLoginClicked() {
        NavDirections action = RegisterFragmentDirections.registerToLogin();
        Navigation.findNavController(getView()).navigate(action);

    }

    @OnClick(R.id.registerRegisterBtn)
    public void onRegister() {
        if(registerDone()){
            NavDirections action = RegisterFragmentDirections.registerToLogged();
            Navigation.findNavController(getView()).navigate(action);

        }else {
            Toast.makeText(getActivity(),"error",Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateEmail() {
        return Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches();
    }

    private boolean registerDone() {
        boolean allDone = true;

        registerUsernameInput.setError(null);
        registerPasswordInput.setError(null);
        registerRepeatPasswordInput.setError(null);
        registerEmailInput.setError(null);
        checkboxBind.setError(null);

        boolean validateUsername = validateNotEmpty(registerUsernameInput);
        boolean validatePassword = validateNotEmpty(registerPasswordInput);
        boolean validateRepeatPassword = validateNotEmpty(registerRepeatPasswordInput);
        boolean validateEmail = validateNotEmpty(registerEmailInput);


        if (!passwordRegister.getText().toString().equals(repeatPassword.getText().toString())){
            allDone = false;
            registerRepeatPasswordInput.setError("Password doesen't match");
            scrollTo(registerRepeatPasswordInput);
        }
       if(!validateEmail()){
            allDone = false;
            registerEmailInput.setError("Not an Email");
        }
        if(!checkboxBind.isChecked()){
            allDone = false;
            checkboxBind.setError("Must accept");
        }

        if(!validateEmail || !validateUsername || !validatePassword || !validateRepeatPassword){
            allDone = false;
        }
        return allDone;
    }

    private boolean validateNotEmpty(TextInputLayout inputLayout) {
        boolean notEmpty = true;
        if(inputLayout.getEditText().getText().toString().isEmpty()){
            notEmpty = false;
            inputLayout.setError("Field Required");
            scrollTo(inputLayout);
        }
        return notEmpty;
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView,targetView);
    }

    @OnClick(R.id.gender)
    public void onGender() {
        optionsArray = new String[4];
        optionsArray[0] = "He/Him";
        optionsArray[1] = "She/Her";
        optionsArray[2] = "They/Them";
        optionsArray[3] = "Rather not say";

        new MaterialAlertDialogBuilder(getContext(), R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered)
                .setTitle("")
                .setItems(optionsArray,this::onGenderSelected)
                .show();
    }

    private void onGenderSelected(DialogInterface dialogInterface, int i) {
        gender.setText(optionsArray[i]);
    }

}
