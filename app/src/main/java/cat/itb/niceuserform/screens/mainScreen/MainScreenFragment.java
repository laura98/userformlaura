package cat.itb.niceuserform.screens.mainScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.niceuserform.R;

public class MainScreenFragment extends Fragment {

    private MainScreenViewModel mViewModel;
    @BindView(R.id.mainLoginBtn)
    Button loginBtn;

    public static MainScreenFragment newInstance() {
        return new MainScreenFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_screen_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainScreenViewModel.class);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.mainLoginBtn)
    public void onMainLoginClicked(){
        NavDirections action = MainScreenFragmentDirections.mainToLogin();
        Navigation.findNavController(getView()).navigate(action);
    }

    @OnClick(R.id.mainRegisterBtn)
    public void onMainRegisterClicked(){
        NavDirections action = MainScreenFragmentDirections.mainToRegister();
        Navigation.findNavController(getView()).navigate(action);

    }


}
