package cat.itb.niceuserform;

import android.content.Context;
import android.view.View;

import androidx.test.annotation.UiThreadTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.google.android.material.textfield.TextInputLayout;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.matcher.ViewMatchers.hasErrorText;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void testLoginCorrecte() {

        onView(withId(R.id.mainLoginBtn)).perform(click());
        onView(withId(R.id.loginBackgroundImg)).check(matches(isDisplayed()));
        onView(withId(R.id.loginName)).perform(replaceText("test"));
        closeSoftKeyboard();
        onView(withId(R.id.loginPassword)).perform(replaceText("test"));
        closeSoftKeyboard();
        onView(withId(R.id.loginLoginBtn)).perform(click());
        onView(withId(R.id.loggedImg)).check(matches(isDisplayed()));
    }
    @Test
    public void testLoginIncorrecte() {
        onView(withId(R.id.mainLoginBtn)).perform(click());
        onView(withId(R.id.loginBackgroundImg)).check(matches(isDisplayed()));
        onView(withId(R.id.loginLoginBtn)).perform(click());
        onView(withId(R.id.loginUsernameInput)).check(matches(hasTextInputLayoutHintText("Field Required")));
        onView(withId(R.id.loginPasswordInput)).check(matches(hasTextInputLayoutHintText("Field Required")));
    }

    @Test
    public void testRegistreCorrecte(){
        onView(withId(R.id.mainRegisterBtn)).perform(click());
        onView(withId(R.id.registerBackgroundImg)).check(matches(isDisplayed()));
        onView(withId(R.id.usernameRegister)).perform(replaceText("test"));
        onView(withId(R.id.passwordRegister)).perform(replaceText("test"));
        onView(withId(R.id.repeatPassword)).perform(replaceText("test"));
        onView(withId(R.id.email)).perform(replaceText("test@test.test"));
        onView(withId(R.id.checkbox)).perform(click());
        onView(withId(R.id.registerRegisterBtn)).perform(click());
        onView(withId(R.id.loggedImg)).check(matches(isDisplayed()));
    }

    @Test
    public void testRegistreIncorrecte(){
        onView(withId(R.id.mainRegisterBtn)).perform(click());
        onView(withId(R.id.registerBackgroundImg)).check(matches(isDisplayed()));
        onView(withId(R.id.registerRegisterBtn)).perform(click());
        onView(withId(R.id.registerUsernameInput)).check(matches(hasTextInputLayoutHintText("Field Required")));
        onView(withId(R.id.registerPasswordInput)).check(matches(hasTextInputLayoutHintText("Field Required")));
        onView(withId(R.id.registerRepeatPasswordInput)).check(matches(hasTextInputLayoutHintText("Field Required")));
        onView(withId(R.id.registerEmailInput)).check(matches(hasTextInputLayoutHintText("Not an Email")));

    }
    @Test
    public void testLoginToRegister(){
        onView(withId(R.id.mainLoginBtn)).perform(click());
        onView(withId(R.id.loginBackgroundImg)).check(matches(isDisplayed()));
        onView(withId(R.id.loginRegisterBtn)).perform(click());
        onView(withId(R.id.registerBackgroundImg)).check(matches(isDisplayed()));
    }

    public static Matcher<View> hasTextInputLayoutHintText(final String expectedErrorText) {
        return new TypeSafeMatcher<View>() {

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextInputLayout)) {
                    return false;
                }

                CharSequence error = ((TextInputLayout) view).getError();

                if (error == null) {
                    return false;
                }

                String errorText = error.toString();

                return expectedErrorText.equals(errorText);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }
}

